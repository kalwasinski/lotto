package pl.akademiakodu.lotto;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LottoGenerator {

    public Set<Integer> generate(){
        Set<Integer> numbers = new TreeSet<>();
        Random rand = new Random();
        while (numbers.size() != 6) {
            numbers.add(rand.nextInt(49) + 1);
        }
         return numbers;
    }
}
