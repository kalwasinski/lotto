package pl.akademiakodu.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    /*
    @ResponseBody, result is a String

     */

    @ResponseBody
    @GetMapping("/")    // path to our main page
    public String hello() {
        return "Hola el mundo";
    }

    @ResponseBody
    @GetMapping("/bye")    // path to our main page
    public String bye() {
        return "Witaj swiecie";
    }

    @GetMapping("/main/")    // path to our main page
    public String welcome() {
        return "Welcome";    // return HTML in resources/templates/welcome.html
    }

    @GetMapping("/product/")    // path to our main page
    public String product() {
        return "product";    // return HTML in resources/templates/welcome.html
    }
    @GetMapping("/lotto")    // path to our main page
    public String generatelotto(ModelMap map) {
        LottoGenerator lottoGenerator = new LottoGenerator();
        map.put("numbers", lottoGenerator.generate());
        return "lotto";    // return HTML in resources/templates/welcome.html
    }


}
